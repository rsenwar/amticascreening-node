const express = require('express')
const app = express();
const router = express.Router();
var mysql = require('mysql');
var DB = require('./app/database/mysql_config.js');
var bodyparser = require('body-parser');
app.use(bodyparser.json());
var cors = require('cors');
app.use(cors());
app.get('/', (err,res) => {
  res.send("Hello World!");
})

app.get('/login', function(req,res) {
    var db = DB.get_mysql_connection();
    var email = req.query.email;
    var password = req.query.password;
    var con = {
        email: email,
        password: password
    }
    console.log(con);
    var query = "select * from user_info where email=? and password=?";
    db.query(query, [email,password], function(error, result) {
        if(error) {
            console.log(error);
            res.status(400).send({success:false});
        } else {
            if(result.length==0) 
                res.send({success:false, message: 'email/password invalid'});
            else {
                res.send({success:true, message: 'success', data:result[0]});
            }
        }
    })
})

app.post('/register', function(req,res) {
    var db = DB.get_mysql_connection();
    var details = req.body;
    console.log(req.body);
    var query1 = "select * from user_info where email = ?";
    var query2 = "insert into user_info set ?";
    db.query(query1, [details.email], function(error, result){
        if(error) {
            console.log(error);
            res.status(400).send({success:false, message:'error'});
        } else {
            if(result.length==0) {
                db.query(query2, [details], function(err, rst){
                    if(err) {
                        console.log(err);
                        res.status(400).send({success:false});
                    } else {
                        res.send({success:true});
                    }
                })
            } else {
                res.send({success:false, message:'Already Registered'});
            }
        }
    })

})
app.listen(8010, () => {
  console.log('Example app listening on port 8010!')
});
